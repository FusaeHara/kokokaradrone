const { src, dest, parallel, watch } = require('gulp');
const $ = require('./modules.js');
const uglify = $.composer($.uglifyes, $.composer);

var connectSSI = require ('connect-ssi');



function css() {
  return src('./src/scss/*.scss')
    .pipe($.sourcemaps.init())
    .pipe($.sass())
    .on('error', $.sass.logError)
    .pipe($.autoprefixer())
    .pipe($.sourcemaps.write())
    .pipe(dest('./dist/css'))
    .pipe(
      $.rename({
        suffix: '.min'
      })
    )
    .pipe($.minifyCSS())
    .pipe(dest('./dist/css'))
    .pipe(
      $.browserSync.reload({
        stream: true,
        once: true
      })
    );
}

function js() {
  return src('./src/js/*.js', { sourcemaps: true })
    .pipe($.plumber())
    .pipe(uglify({ output: { comments: /^!/ } }))
    .pipe(
      $.concat('main.min.js', {
        newLine: '\n'
      })
    )
    .pipe(dest('./dist/js', { sourcemaps: true }))
    .pipe(
      $.browserSync.reload({
        stream: true,
        once: true
      })
    );
}

function img() {
  return src('./src/img/**')
    .pipe($.changed('./dist/img/'))
    .pipe(
      $.imagemin({
        optimizationLevel: 3
      })
    )
    .pipe(dest('./dist/img/'));
}

function bs() {
  $.browserSync.init({
    server: {
      baseDir: './dist/',
      middleware: [
        connectSSI({
          baseDir: './dist/',
          ext: '.html'
        })
      ],
    },
    notify: true,
    xip: false
  });
}

exports.css = css;
exports.js = js;
exports.bs = bs;
exports.img = img;

exports.default = parallel([css, js, img, bs], () => {
  watch('./src/scss/**', css);
  watch('./src/js/**', js);
  watch('./src/img/**', img);
});
