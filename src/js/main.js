
//clickMap responsible [jquery]

$(document).ready(function(e) {
  $('img[usemap]').rwdImageMaps();
  $(".l-body").fadeIn(200);
});


// user-scalable=no



//*
//* gsap animation
//*


const tl = new TimelineMax();

tl.set('#heroBg', { 
	autoAlpha: 0
})

 .set('.cut1', { 
  autoAlpha: 0
})

 .set('.cut2', {
  autoAlpha: 0
})

.set('.cut3', {
  autoAlpha: 0
})

.set('.cut4', {
  autoAlpha: 0
})

.set('.cut5', {
  autoAlpha: 0
})

.set ('.mcopy', {
  autoAlpha: 0
})


.to('.cut1', 0.6  , {
  autoAlpha: 1,
  ease:Elastic.easeOut
})

.to('#heroBg' , 2.0 ,{
	autoAlpha: 1,	
})
  

.to('.cut2', 0.4 , {
  autoAlpha: 1,
  y: -20,
  ease:Elastic.easeOut
}, '-=0.1')

.to('.cut3',  {
  autoAlpha: 1,
  y: -20,
  ease:Elastic.easeOut
}, '-=0.14')

.to('.cut4',  {
  autoAlpha: 1,
  y: -20,
  ease:Elastic.easeOut
}, '-=0.26')

.to('.cut5',  {
  autoAlpha: 1,
  y: -20,
  ease:Elastic.easeOut
}, '-=0.28')

.to('.mcopy',  {
  autoAlpha: 1,
  ease: Expo.out
},'-=0.68');



//*
//* slider
//*


$( document ).ready(function($) {
	$('#schoolSlides').sliderPro({
	  width: "100%",
	  centerImage: true,
	  autoHeight: true,
	  arrows: true,//左右の矢印
	  buttons: false,//ナビゲーションボタン
	  slideDistance:0,//スライド同士の距離
	  thumbnailTouchSwipe: true,
	});

  });



//*
//* もっと見るボタン
//*

var moreNum = 2; //初期表示件数
var moreNumsub = 5; //２回目以降のmore
$('#detailReview li:nth-child(n + ' + (moreNum + 1) + ')').addClass('is-hidden');
$('#showMore').click(function() {
  $('#detailReview li.is-hidden').slice(0, moreNumsub).removeClass('is-hidden');
  if ($('#detailReview li.is-hidden').length == 0) {
    $('#showMore').fadeOut();
  }
});





//*
//*  pull down
//* 

$(function(){
		//都道府県セレクトボックス
		$("#select-pulldown").on('click',function(){
			
			$("#selectChild").toggle();//1st表示
		});
		
		$(".select-child--1st label").on('click',function(){
			$(".select-child--2nd").css('display','none'); //2st全て非表示表示
			var ClickID = $(this).data("area");
			$(".select-child--1st label").removeClass("selected")
			$(this).addClass("selected");
			switch (ClickID) {
				case "hokkaido":
					$(".hokkaido").css('display','block'); //2st表示
				break;
				case "tohoku":
					$(".tohoku").css('display','block'); //2st表示
				break;
				case "kanto":
					$(".kanto").css('display','block'); //2st表示
				break;
				case "hokuriku":
					$(".hokuriku").css('display','block'); //2st表示
				break;
				case "koshinetsu":
					$(".koshinetsu").css('display','block'); //2st表示
				break;
				case "tokai":
					$(".tokai").css('display','block'); //2st表示
				break;
				case "kinki":
					$(".kinki").css('display','block'); //2st表示
        break;
        case "chugoku":
					$(".chugoku").css('display','block'); //2st表示
        break;
        case "shikoku":
					$(".shikoku").css('display','block'); //2st表示
        break;
        case "kyushu":
					$(".kyushu").css('display','block'); //2st表示
		break;
			}
		});
		
		//$('.select-child--2nd .select-child__label').on('click',function(){
		$('.select-child--2nd input[name="pref[]"]').change(function() {
			PCinputData(this);
		});
		
		function PCinputData(elm){
			var thisParent = $(elm).parents('.select-child--2nd');
			var classVals =$(thisParent).attr('class');
			classVal = classVals.split(' '); // 取得した値を分割
			if ($(elm).prop('checked')) {
				checkCommnPC(elm,true);
				var elm = $(".select-child--1st label[data-area='"+classVal[1]+"'] .checkArea")
				if(elm.length == 0){
					$(".select-child--1st label[data-area='"+classVal[1]+"']").append('<span class="checkArea"><!--選択中--></span>');	
				}
				pulldownMS();
			}else{
				checkCommnPC(elm,false);
				var inputs  = $(thisParent).find("input");
				var flag = false;
				
				//他のグループでチェックがあるか確認
				$(inputs).each(function(index, element){
					if($(element).prop('checked')){
						//チェックあり
						flag = true;
					}
				});
				//全てのチェックがなければ「選択中」を削除
				if(flag == false){
					$(".select-child--1st label[data-area='"+classVal[1]+"'] .checkArea").remove();
					pulldownMS();
				}
			}
		}
		
	//初期表示
	function pulldownMS(){
		//選択中の項目を取り出す
		var findCheckArea = $(".select-child--1st .checkArea");
		var num = 0;
		var areaName = ""
		$(findCheckArea).each(function(index, element){
			if(index == 0){
				areaName = $($(element).prev("span")).text();
			}
			
		})
		if(findCheckArea.length == 1){
			$("#select-pulldown").text(areaName);
		}else if(findCheckArea.length > 1){
			var num = findCheckArea.length-1;
			$("#select-pulldown").text(areaName+", 他"+num+"件" );
		}else{
			$("#select-pulldown").text("- なし");
		}
	}
	
	
	
	
	/*SP対応*/
	$("#select-pulldown--sp").on('click',function(){
		$("#selectChildsp").css("display",'block');
		return false;
	});
	
	$(".DecisionBtn").on('click',function(){
		$("#selectChildsp").css("display",'none');
	});
	
	$('.select-child--sp__2ndlist input[name="pref[]"]').change(function() {
		if ($(this).prop('checked')) {
			checkCommnSP(this,true);
		}else{
			checkCommnSP(this,false);
		}
		pulldownSPMS();
	});
	
	function pulldownSPMS(){
		var lists = $(".select-child--sp__2ndlist");
		var Names = [];
		$(lists).each(function(index, element){
			var inputs = $(element).find("input");
			var checkDAta = false;
			$(inputs).each(function(index, input){
				
				if($(input).prop('checked')){
					console.log("スマホ_チェックあり")
					checkDAta = true;
				}else{
					console.log("スマホ_チェックなし")
				}
			});
			if(checkDAta){
				var areaName = $($(element).prev("h3")).text();
				Names.push(areaName)	
			}
			
		});
		
		//表示の反映
		if(Names.length == 1){
			$("#select-pulldown--sp").text(Names[0]);
		}else if(Names.length > 1){
			var num = Names.length-1;
			$("#select-pulldown--sp").text(Names[0]+", 他"+num+"件" );
		}else{
			$("#select-pulldown--sp").text("- なし");
		}
		console.log(Names);
	}
	
	//PC選択した場合はスマホに反映
	function checkCommnPC(elm,checkFlag){
		var nextElm = $(elm).next();
		var text = $(nextElm).text();	//該当県名テキスト取得
		
		//スマホ
		var SPspnas =  $(".select-child--sp__2ndlist span");
		$(SPspnas).each(function(index, SPspna){
			var spText = $(SPspna).text();
			if(text == spText){			//スマホのテキストとcheckされたテキストを比較
				var inptElm = $(SPspna).prev();
				if(checkFlag == true){
					$(inptElm).prop("checked",true);	//該当箇所にcheckを入れる	
				}else{
					$(inptElm).prop("checked",false);	//該当箇所にcheckを入れる	
				}
				pulldownSPMS();
				
			}
		});
	}
	
	//スマホ選択した場合はPCに反映
	function checkCommnSP(elm,checkFlag){
		var nextElm = $(elm).next();
		var text = $(nextElm).text();
		
		//PC
		var PCspnas =  $(".select-child--2nd span");
		$(PCspnas).each(function(index, PCspna){
			var pcText = $(PCspna).text();
			if(text == pcText){			//スマホのテキストとcheckされたテキストを比較
				var inptElm = $(PCspna).prev();
				if(checkFlag == true){
					$(inptElm).prop("checked",true);	//該当箇所にcheckを入れる	
					PCinputData(inptElm);
				}else{
					$(inptElm).prop("checked",false);	//該当箇所にcheckを入れる	
					PCinputData(inptElm);
				}
			}
		});
	}


	//ブランド
	$('input[name="brand[]"]').change(function() {
		var idName = $(this).attr("id");
		var SPid ="#sp"+idName;	//スマホ IDに変更
		//スマホにチェック
		if($(this).prop('checked')){
			checkChange(SPid,true);
		}else{
			checkChange(SPid,false);
		}
		pulldownSPMS()
	});
	$('input[name="spbrand[]"]').change(function() {
		var idName = $(this).attr("id");
		var PCid ="#"+idName.slice( 2 ) ;
		if($(this).prop('checked')){
			checkChange(PCid,true);
		}else{
			checkChange(PCid,false);
		}
		pulldownSPMS()
	});
	
	//ライセンス
	$('input[name="license[]"]').change(function() {
		var idName = $(this).attr("id");
		var SPid ="#sp"+idName;	//スマホ IDに変更
		//スマホにチェック
		if($(this).prop('checked')){
			checkChange(SPid,true);
		}else{
			checkChange(SPid,false);
		}
		pulldownSPMS()
	});
	$('input[name="splicense[]"]').change(function() {
		var idName = $(this).attr("id");
		var PCid ="#"+idName.slice( 2 ) ;
		if($(this).prop('checked')){
			checkChange(PCid,true);
		}else{
			checkChange(PCid,false);
		}
		pulldownSPMS()
	});
	
	
	function checkChange(id,checkFlag){
		if (checkFlag) {
			$(id).prop("checked",true);	//該当箇所をON	
		}else{
			$(id).prop("checked",false);//該当箇所をOFF	
		}
	}
	
	
})